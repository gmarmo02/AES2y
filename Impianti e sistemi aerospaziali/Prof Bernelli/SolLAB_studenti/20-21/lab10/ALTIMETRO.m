clc, clear, close all,
%% DATI
h_mi = 110;             %[m] altezza Milano
h_ma = 610;             %[m] altezza Madrid
Ts_mi = 26 + 273.15;    %[K] Temperatura suolo Milano
Ts_ma = 16 + 273.15;    %[K] Temperatura suolo Madrid
p_mi = 101800;          %[Pa] Pressione Milano QNH
p_ma = 100900;          %[Pa] Pressione Madrid QNH
FL = 360;               %[100ft] fligh level

% ARIA TIPO
R = 287;                %[J/kg*K] 
a = 0.0065;             %[K/m] gradiente di temperatura
g = 9.81;               %[m/s^2] accelerazione di gravità
T0 = 288.16;            %[K] temperatura standard
p0 = 101325;            %[Pa] pressione standard
z = [0 500 610 (1000:1000:11000)]; %QUOTE PER PUNTO 2
%% 1. CONDIZIONI METEO TEORICHE AL MSL NEI DUE AEROPORTI
Tm_mi = Ts_mi + a*h_mi; %[K] Temperatura teorica livello mare Milano
Tm_ma = Ts_ma + a*h_ma; %[K] Temperatura teorica livello mare Madrid

p1_mi = p_mi*(1-a*h_mi/T0)^(g/(R*a)); %pressione altimetrica con errore di temperatura
p1_ma = p_ma*(1-a*h_ma/T0)^(g/(R*a)); %pressione altimetrica con errore di temperatura

pm_mi = p1_mi/((1-a*h_mi/Tm_mi)^(g/(R*a)));
pm_ma = p1_ma/((1-a*h_ma/Tm_ma)^(g/(R*a)));

% p_ICAO = p0*(1-a*z/T0).^(g/R*a);
% p_MILANO = p0*(1-a*z/Tm_mi).^(g/R*a);
% p_MADRID = p0*(1-a*z/Tm_ma).^(g/R*a);
% figure(3), plot(p_ICAO,z,'k',p_MILANO,z,'r',p_MADRID,z,'b')
% legend('ICAO','MILANO','MADRID')

fprintf('(1)CONDIZIONI METEO TEORICHE AL LIVELLO MEDIO MARE:\n')
fprintf('Milano: Tm = %g [°C], Pm = %g [Pa]\n',Tm_mi-273.15,pm_mi)
fprintf('Madrid: Tm = %g [°C], Pm = %g [Pa]\n',Tm_ma-273.15,pm_ma)

%% 2. DIAGRAMMI SIGNIFICATIVI QFE; QNE; QNH (MADRID)
T = Tm_ma - a*z;
P = pm_ma*(1-a*z/Tm_ma).^(g/(R*a));

p_QFE = p1_ma;
z_QFE = (T0/a) *(1-(P/p_QFE).^((R*a)/g));
err_QFE = z_QFE - z ;

p_QNH = p_ma;
z_QNH = (T0/a) *(1-(P/p_QNH).^((R*a)/g));
err_QNH = z_QNH - z;

p_QNE = p0;
z_QNE = (T0/a) *(1-(P/p_QNE).^((R*a)/g));
err_QNE = z_QNE - z;


figure(1), plot(P,z,'k',P,z_QFE,'r',P,z_QNH,'*',P,z_QNE,'--','lineWidth',1.4)
legend('QUOTA REALE','QUOTA QFE','QUOTA QNH','QUOTA QNE')
grid on
xlabel('PRESSIONE [Pa]')
ylabel('QUOTA [m]')

figure(2), plot(z,err_QFE,'r',z,err_QNH,'*',z,err_QNE,'--',[z(1) z(end)],[0 0],'k')
legend('QFE','QNH','QNE')
grid on
%% 3. 

p_FL = p0 * (1-(a*FL*100*0.305/T0))^(g/(R*a));      %Pressione Livello di volo
z_FL_mi = Tm_mi/a * (1-(p_FL/pm_mi)^(R*a/g));       %Quota reale corrispondete al flight level a Milano
z_FL_ma = Tm_ma/a * (1-(p_FL/pm_ma)^(R*a/g));       %Quota reale corrispondete al flight level a Madrid

%CABINA

% z_cabina = 







