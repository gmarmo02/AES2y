close all
clear all
a = 0.0065;
R = 287;
T0 = 288.16;
gamma = 1.4;
T = @(z) T0-a*z.*(z<11000) + T0-a*11000*(z>=11000);
c = @(z) sqrt(gamma*R*T(z));

z = [0:15000];
plot(c(z),z)
grid on
hold on