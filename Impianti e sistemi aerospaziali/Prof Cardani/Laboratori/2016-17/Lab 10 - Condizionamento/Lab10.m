m_dot2 = 300/3600; %mass flow rate (air)
m_dot6 = m_dot2;
p1 = 101325;
beta = 3.2; % p2/p1
p2 = beta*p1;
p3 = 0.95*p2;
p4 = p1; p5 = p1; p6 = p1;
p7 = 0.95*p1;
T4 = 277.15;

gammaA = 1.05;
gammaB = 1.37;

epsilon = 0.8; % efficacia scambiatore
ethaC = 0.9; % rendimento compressore
ethaT = 0.9; % rendimento turbina


%% CASO 1: T1 = 283.15K, T6 = 298.15K
T1 = 283.15;
T6 = 298.15;


T2id = T1 * beta^(0.4/1.4)
T2 = (T2id-T1)/ethaC + T1

T5 = T2*(p2/p5)^((1-gammaA)/gammaA)
T7 = T1*(p1/p7)^((1-gammaB)/gammaB)

T3 = T4/(ethaT*(T1/T2id - 1)+1) % ciclo simmetrico
T3 = T4 / (1-ethaT + ethaT*(p4/p3)^(.4/1.4)) % soluzione

m_dot5 = m_dot6*(T6-T4)/(T5-T4)
m_dot4 = m_dot6 - m_dot5
m_dot3 = m_dot4

m_dot5/m_dot4

m_dot7 = m_dot3*(T2-T3)/(epsilon*(T2-T7))

T8 = (m_dot3/m_dot7)*(T2-T3) + T7




%% CASO 2: T2 = 303.15K, T6 = 293.15K
T1 = 303.15;
T6 = 293.15;

T2id = T1 * beta^(0.4/1.4)
T2 = (T2id-T1)/ethaC + T1

T5 = T2*(p2/p5)^((1-gammaA)/gammaA)
T7 = T1*(p1/p7)^((1-gammaB)/gammaB)

T3 = T4/(ethaT*(T1/T2id - 1)+1) % ciclo simmetrico
T3 = T4 / (1-ethaT + ethaT*(p4/p3)^(.4/1.4)) % soluzione

m_dot5 = m_dot6*(T6-T4)/(T5-T4)
m_dot4 = m_dot6 - m_dot5
m_dot3 = m_dot4

m_dot5/m_dot4

m_dot7 = m_dot3*(T2-T3)/(epsilon*(T2-T7))

T8 = (m_dot3/m_dot7)*(T2-T3) + T7
