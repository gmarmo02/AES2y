clc
echo on

%%%
%%% ESERCIZIO 1
%%%

%Definizione del sistema dinamico
 G=tf(10*[-1 1],conv([1 1],[.1 1]))
 pause
 
%Tracciamento dei diagrammi di Bode
 bode(G)
 pause

%Risposta alla sinusoide 
t=0:0.01:10;
y=lsim(G,sin(10*t),t);
pause

%Calcolo della risposta in frequenza per w=10
G10=freqresp(G,10)
pause

%Calcolo risposta asintotica e confronto
yas=abs(G10)*sin(10*t+angle(G10));
plot(t,y,t,yas);
pause
   		
%%%
%%% ESERCIZIO 2
%%%

%Definizione del sistema dinamico
G=tf(1,[1 1.4 1])
pause
        
%Tracciamento dei diagrammi di Bode
 bode(G)
 pause

%Risposta alla somma di sinusoidi        
t=0:0.01:100;
u = sin(0.4*t)+ 0.2*sin(10*t);
y=lsim(G,u,t);
plot(t,u,t,y)
pause
	
%Risposta all'onda quadra
[u,t]=gensig('square',15);
y=lsim(G,u,t);
plot(t,u,t,y);
pause

%%%
%%% ESERCIZIO 3
%%%

%Definizione delle funzioni di trasferimento

numg1=80;
deng1=[1 0.8 16];
g1=tf(numg1,deng1)
pause
omega_n1=sqrt(16);
csi_1=0.8/(2*omega_n1);
        
          
numg2=80;
deng2=[1 7.2 16];
g2=tf(numg2,deng2)
pause
omega_n2=sqrt(16);
csi_2=7.2/(2*omega_n2);
		
numg3=80;
deng3=[1 0 16];
g3=tf(numg3,deng3)
pause
omega_n3=sqrt(16);
csi_3=0/(2*omega_n3);

%Diagrammi di Bode

bode(g1)
grid on
pause
        
hold on
bode(g2,'r')
legend('g1','g2')
pause
        
hold on
bode(g3,'k')
legend('g1','g2','g3')
pause
        
%Diagrammi polari (di Nyquist)
figure
nyquist(g1)
hold on
pause
        
nyquist(g2,'r')
legend('g1','g2')
pause
        
nyquist(g3,'k')
legend('g1','g2','g3')
pause
        
 %Risposte allo scalino

figure
step(g1)
grid on
pause
       
hold on
step(g2,'r')
legend('g1','g2')
pause
        
hold on
step(g3,'k')
legend('g1','g2','g3')
