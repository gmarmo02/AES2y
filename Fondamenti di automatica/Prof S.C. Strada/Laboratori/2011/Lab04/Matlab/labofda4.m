clc
echo on
%
% Controllo automatico dell'altitudine in un Boeing 747
%
%

% Definizione del modello lineare per controllo di altitudine

A=zeros(5,5);

A(1,:)=[-0.00643 0.0263 0 -32.2 0];
A(2,:)=[-0.0941 -0.624 820 0 0];
A(3,:)=[-0.000222 -0.00153 -0.668 0 0];
A(4,:)=[0 0 1 0 0];
A(5,:)=[0 -1 0 830 0];

b=[0 -32.7 -2.08 0 0]';

c=[0 0 0 0 1];

b747=tf(ss(A,b,c,0))
pause

% Poli del sistema. 

damp(b747)
pause

%Poli e zeri nel piano complesso
pzmap(b747)
pause


% Risposte allo scalino e all'impulso
step(b747)
pause

impulse(b747)
pause

%Diagrammi di Bode e polare
bode(b747)
pause

nyquist(b747)
pause

% Chiusura di un anello di retroazione unitario
margin(-b747);
pause

% Progetto per tentativi del regolatore PD
% 1� tentativo 
KP=1;
KD=10*KP;
margin(series(b747,tf(-[KD KP],1)));
pause

% 2� tentativo
KP=0.1;
KD=10*KP;
margin(series(b747,tf(-[KD KP],1)));
pause

% 3� tentativo
KP=0.01;
KD=10*KP;
margin(series(b747,tf(-[KD KP],1)));
pause

% 4� tentativo
KP=0.001;
KD=10*KP;
margin(series(b747,tf(-[KD KP],1)));
pause

% 5� tentativo
KP=0.0001;
KD=10*KP;
margin(series(b747,tf(-[KD KP],1)));
pause

% 6� tentativo
KP=0.00005;
KD=10*KP;
margin(series(b747,tf(-[KD KP],1)));
pause

% Verifica della risposta ottenuta 
% in anello chiuso ad un gradino di
% riferimento di altitudine

step(feedback(series(b747,tf(-[KD KP],1)),1));









