%Labo12parte2ese1
clear all
close all
clc
%domanda 1
ys=[ 10.0 9.92 9.79 9.55 9.23 8.77 8.20 7.52 6.80 6.01 5.10 ];
x=0:0.1:1;
f=@(t)10-0.5*9.81*t.^2;
t=linspace(min(x),max(x),1000);
y=f(t);
%figure(1)
%plot(x,ys,x,f(x))

%err=abs(ys-f(x));
%figure(2)
%plot(x,err)

%domanda 2
%lagrange
xval=linspace(min(x),max(x),1000);
nodi=length(ys);
grado=nodi-1;
P=polyfit(x,ys,grado);
polilag=polyval(P,xval);

%lin-composita
polilc=interp1(x,ys,xval);

%minimi quadrati
pm=polyfit(x,ys,2);
poliquad=polyval(pm,xval);

%errori
errlag=abs(y-polilag);
errilc=abs(y-polilc);
errquad=abs(y-poliquad);

figure(1)
plot(xval,errlag,xval,errilc,xval,errquad)
