%labo 13 ese 1
clear all
close all
clc
%domanda 1
a=-3;
b=+3;
f=@(x)exp(-x.^2).*sin(2.*x+1);
x=linspace(a,b,1000);
y=f(x);

df=@(x)2.*exp(-x.^2).*(cos(2.*x+1)+x.*sin(2.*x+1));
ydf=df(x);

figure(1)
plot(x,y,x,ydf,'linewidth',2)
grid on

%domanda 2
H=[0.4 0.2 0.1 0.05 0.025 0.0125];
x0=0;
for i=1:length(H)
    h=H(i) ;   
    xa(i)=(f(x0+h)-f(x0))./h;
    xi(i)=(f(x0)-f(x0-h))./h;
    xc(i)=(f(x0+h)-f(x0-h))./(2.*h);
end

%domanda 3
dfx0=df(x0);
erra=abs(dfx0-xa);
erri=abs(dfx0-xi);
errc=abs(dfx0-xc);

figure(1)
loglog(H,erra,'g')
hold on
loglog(H,erri,H,errc,H,H,H,H.^2)
grid on

%domanda 4
h=0.1;
xval=[-3:h:+3];
fval=f(xval);

%disegno la derivata!
xc1=(fval(3:end)-fval(1:end-2))./(2.*h);
figure(2)
plot(xval(2:end-1),xc1,'^');
grid on

%domanda 6
A=3/2;
B=-2;
C=1/2;

D=A*f(x0)+B*f(x0-h)+C*f(x0-2.*h);
for i=1:length(H)
    h=H(i);
    Df=A*f(x0)+B*f(x0-h)+C*f(x0-2.*h);
    
end
ErrN = abs(Df-dfx0);

