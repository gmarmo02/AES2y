%% Labo21

clear all
close all
clc

%% Domanda 1
uesatta=@(x)sin(x./2).*cos(x);
duesatta=@(x)0.5.*cos(x./2)*cos(x)-sin(x./2).*sin(x);

q=3;
a=-4;
b=+4;

forzante=@(x)(5/4+q).*sin(x./2).*cos(x)+cos(x./2).*sin(x);

h=0.05;
us=uesatta(a);
l=duesatta(b);

n=(8/h)-1;
xn=linspace(a,b,n+2);

du=-ones(n-1,1)/h^2;
dl=du;
d0=(2+q*h^2)*ones(n,1)/h^2;

b=forzante(xn(2:end-1));
b(1)=b(1)+us/h^2;

%matrice e termine modificate per newman;
d0=[d0;1/h^2];
dl=[dl;-1/h^2];
du=[du;-1/h^2];
b=[b,l/h];

[LT,UT,u]=thomas3diag(du,dl,d0,b);
u=[us;u];

figure(1)
plot(xn,u,'^g')
hold on
grid on
plot(xn,uesatta(xn),'linewidth',2)


