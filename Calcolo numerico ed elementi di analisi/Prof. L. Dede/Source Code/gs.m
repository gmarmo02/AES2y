function [x,k]=gs(A,b,x0,nmax,toll)

if nargin==3
    toll=1e-6;
    nmax=1000;
end

n=length(b);
x=zeros(n,1);
k=0;

L=tril(A);
xv=x0;
r=b-A*x0;
err=norm(r)/norm(b);

while err>toll && k<nmax
    k=k+1;
    z=forward(L,r);
    x=xv+z;
    r=b-A*x;  
    err=norm(r)/norm(b);
    xv=x;
end
end

