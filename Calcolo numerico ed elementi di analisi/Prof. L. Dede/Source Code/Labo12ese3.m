%labo 12 ese3
clear all
close all
clc
%domanda 1
a=-pi*2;
b=+pi*2;
f=@(x)sin(1./(1+x.^2));
x=linspace(a,b,1000);
y=f(x);

n=10;
h=(b-a)/n;
xval=[a:h:b];
fval=f(xval);
grado=length(xval)-1;

P=polyfit(xval,fval,grado);
Polilag=polyval(P,x);

%domanda 2
err=abs(Polilag-y);
errmax=max(abs(Polilag-y));

figure(1)
plot(x,y,x,Polilag)

figure(2)
plot(x,err)

%domanda 3

N=[4 8 10];
for z=N
    i=[0:z];
    xi=-cos((pi/z)*i);
    nodi=(a+b)/2+((b-a)/2)*xi;
    
    p2=polyfit(nodi,f(nodi),z);
    Poliche=polyval(p2,x);
end

errch=abs(Poliche-y);

figure(3)
plot(x,errch)

figure(4)
plot(x,Poliche,x,y,'linewidth',2)

