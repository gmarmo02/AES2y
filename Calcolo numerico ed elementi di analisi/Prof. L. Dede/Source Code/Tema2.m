%% Tema 2
clear all
close all
clc

%% Esercizio 1 Domanda 1

N=30;
A=diag(2:2:2*N)+diag(1./(2:N),1)+diag(1:(N-1),-1);
xes=ones(N,1);
b=A*xes;
%% Esercizio 1 domanda 4
da=diag(A);
na=size(A,1);
for j=1:na
    r(j)=abs(da(j))-sum(abs(A([1:j-1,j+1:na], j)));
end
if(r>0)
    disp('A=dominanza diagonale')
end

%% Esercizio 1 domanda 5
x0=[5:5:150]';
toll=1e-5;

[xn,it]=jacobi(A,b,x0,toll);

errrel=norm(xn-xes)/norm(xes);
res=norm(b-A*xn)/norm(b);
cond(A);
pause

%% Esercizio 2 domanda 3
clear all
close all
clc

f=@(x)sin(x);

a=0;
b=(3/4)*pi;
n2=((b-a).^3/(10.^-4*24)).^0.5;
n=ceil(n2)
[i]=Tema2puntomedio(a,b,n,f);

ies=1+1/2.^0.5;
pause
%% Esercizio 3
clear all
close all
clc
h=0.5;
ues=@(t)sin(t).*exp(-t/2);
f=@(t,y)cos(t).*exp(-t/2)-0.5*y;

tdis=linspace(0,10,1000);

[th,uh]=Tema2Ei(f,10,0,h);

figure (1)
plot(th,uh,'ro-',tdis,ues(tdis),'b')
pause
%% Esercizio 4
clear all
close all
clc
f=@(x)sin(exp(x)).*exp(x).^2-cos(exp(x)).*exp(x);

uex=@(x)sin(exp(x));
duex=@(x)cos(exp(x)).*exp(x);

N=[20 40 80 160];
err=[];
H=[];

for n=N
    aa=-3;
    bb=0.5;
    
    h=(bb-aa)/(n +1);
    H=[H;h];
    
    xn=linspace(aa,bb,n+2);
    d0=(2/h.^2)*ones(n,1);
    du=(-1/h.^2)*ones(n-1,1);
    
    b=f(xn(2:end-1));
    s=duex(aa);
    ud=uex(bb);
    
    b(1)=b(1);
    b(end)=b(end)+ud/h.^2;
    b=[-s/h,b];
    
    d0=[1/h^2;d0];
    du=[-1/h^2;du];
    
    A=diag(d0)+diag(du,1)+diag(du,-1);
    u=A\b';
    
    uu=[u;ud];
    err=[err;max(abs(uex(xn)'-uu))];

end
x2=linspace(aa,bb,1000);
figure(2)
plot(xn,uu,'^g')%xn,uex(xn),'linewidth',2);
hold on
plot(x2,uex(x2),'linewidth',2);

figure(3)
loglog(H,err,H,H,H,H.^2);
grid on
legend('Err','H','H^2')
