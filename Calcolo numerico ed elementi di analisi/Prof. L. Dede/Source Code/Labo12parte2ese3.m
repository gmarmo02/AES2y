%labo 12 parte 2 ese 3
clear all
close all
clc
%domanda 1
a=-1;
b=+1;
f=@(x)abs(x-pi/12);
x=linspace(a,b,100000);

%domanda 2
n1=5;
P1=polyfit(x,f(x),n1);
polilag5=polyval(P1,x);

n2=10;
P2=polyfit(x,f(x),n2);
polilag10=polyval(P2,x);

n3=15;
P3=polyfit(x,f(x),n3);
polilag15=polyval(P3,x);

err5=abs(f(x)-polilag5);
err10=abs(f(x)-polilag10);
err15=abs(f(x)-polilag15);

%figure(2)
%plot(x,err5,x,err10,x,err15)

%figure(1)
%plot(x,f(x),x,polilag5,'linewidth',2)
%grid on

%domanda 3

z=[2:2:26];
A=[];
Err=[];
Errmax=[];
for w=z
    i=[0:w];
    xi=-cos((pi/w)*i);
    nodi=(b+a)/2+((b-a)/2)*xi;
    
    P=polyfit(nodi,f(nodi),w);
    poliche=polyval(P,x);
    A=[A;poliche];
    Err=[Err;abs(f(x)-poliche)];
    Errmax=[Errmax;max(abs(f(x)-poliche))];
end

figure(3)
loglog(z,Errmax)
grid on