%%Esercitazione 5 e 6

resetall

%%Ese 1
%definisco la funzione
f= @(x) x.^3 - (2+exp(1))*x.^2 + (2*exp(1)+1)*x + (1-exp(1)) - cosh(x-1);
%definisco l'intervallo
x=linspace(0.5,6.5,100);
%faccio la funzione
y=f(x);
y0=zeros(size(x));
%disegni
grid on
hold on
figure (1)
plot(x,y,'g',x,y0,'r');
%la funzione presenta degli zeri nelle vicinanze di 1,3 e 6,5
%controllo se si puo' applicare la bisezione
%prima radice
if(f(0.5)*f(1.5)<0);
    fprintf('ok\n');
else
    fprintf('no\n');
end
%seconda radice
if((f(3)*f(4))<0);
    fprintf('ok\n');
else
    fprint('no\n');
end
%terza radice
if(f(6)*f(6.5)<0);
    fprintf('ok\n');
else
    fprint('no\n');
end
%applico il metodo alle ultime 2 radici;
%algoritmo di bisezione seconda radice
a=3;
b=4;
toll=10^-6;
[xvect,it]=bisezione(a,b,toll,f);
%algoritmo di bisezione terza radice
c=6;
d=6.5;
tolle=10^-6;
[xvect1,it1]=bisezione(c,d,tolle,f);

%%Esercizio 2
fprimo=@(x) 3*x.^2 - 2*(2+exp(1))*x + (2*exp(1)+1) - sinh(x-1);
figure(2);
yprimo=f(x);
figure(2)
plot(x,y,x,y0,x,yprimo);
