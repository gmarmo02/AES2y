%% Labo 20
clear all
close all
clc

%% Domanda 1
aa=-4;
bb=+4;
q=3;

uesatta=@(x)sin(x./2).*cos(x);
f=@(x)(5/4+q).*sin(x./2).*cos(x)+cos(x./2).*sin(x);

us=f(aa);
ud=f(bb);

H=[0.5 0.25 0.125 0.0625 0.03125];
err=[];
for h=H
    x=[aa+h:h:bb-h];
    xv=aa:h:bb;
    n=((bb-aa)/h)-1;
    
    d0=(2+q*h.^2)*ones(n,1)./h.^2;
    dlower=-1*ones(n-1,1)./h.^2;
    dupper=dlower;
    
    b=f(x);
    b(1)=us./h^2;
    b(end)=+ud./h^2;
    u2=zeros(1,n);
    
    [LT1,UT1,u2]=thomas3diag(dupper,dlower,d0,b);
    uu1=[us; u2; ud];

end


figure(1)
plot(xv,uu1,'linewidth',2)