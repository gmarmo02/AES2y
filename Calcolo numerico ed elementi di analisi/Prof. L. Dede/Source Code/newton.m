function [xvect,it]=newton(fun,dfun,toll,x0,nmax,mol)

if(nargin==5)
    mol=1;
end

err=1;
it=0;
xvect=[];
xval=x0;

while (it< nmax && err> toll)
    dfx=dfun(xval);
    if dfx==0
        error(' Arresto per azzeramento di dfun');
    else
        xnew=xval-mol*fun(xval)/dfx;
        err=abs(xnew-xval);
        xvect=[xvect; xnew];
        it=it+1;
        xval=xnew;
    end
end