function fn = lotka(t,y) 
[n,m] = size(y);
fn = zeros(n,m);
fn(1) = y(1) * (1 - y(2)); 
fn(2) = y(2) * (y(1) - 1); 
return