n=1000;

A=hilb(n);
cond(A)
x_ex=ones(n,1);
b=A*x_ex;

B=rand(n);
cond(B)
y_ex=ones(n,1);
c=B*y_ex;

x=A\b;
norm(x-x_ex)/norm(x_ex)


y=B\c;
norm(y-y_ex)/norm(y_ex)
