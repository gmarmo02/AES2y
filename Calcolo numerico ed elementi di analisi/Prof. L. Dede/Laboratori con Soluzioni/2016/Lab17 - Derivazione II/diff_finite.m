function deltaf = diff_finite(f,x,h,c)

deltaf = (c(1)*f(x+2*h) + c(2)*f(x+h) + c(3)*f(x) + c(4)*f(x-h) + c(5)*f(x-2*h))/h;
