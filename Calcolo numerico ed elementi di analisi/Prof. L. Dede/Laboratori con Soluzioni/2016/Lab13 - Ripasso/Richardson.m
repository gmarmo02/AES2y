function [x, it] = richardson(A, b,x0,alpha, maxit, toll)

x = x0;
it = 0;
r    = b - A * x;
res  = norm(r) / norm(b);

while ((res > toll) && (it < maxit))
     z = r;
     x = x + alpha * z;
     r = b - A * x;
     res  = norm(r) / norm(b);
     it = it + 1;
end

