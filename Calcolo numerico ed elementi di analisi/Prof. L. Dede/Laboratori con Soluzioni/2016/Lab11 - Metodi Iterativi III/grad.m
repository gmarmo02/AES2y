function [x,iter] = grad(A,b,x0,nmax,toll,alpha_in,Prec)

% Metodo del gradiente
%
% Parametri di ingresso:
%
% A: matrice di sistema
% b: vettore termine noto
% x0: guess iniziale
% tol:  tolleranza criterio d'arresto
% nmax: numero massimo di iterazioni ammesse
% alpha_in: passo da utilizzare per Richardson stazionario. Assegnare NaN
% per usare il metodo del gradiente (Richardson dinamico)
% Prec: precondizionatore del metodo. Parametro opzionale: se non viene passato si usa P=I (gradiente non precondizionato)
%
% Parametri di uscita:
%
% x: matrice le cui colonne sono i vettori della soluzione ad ogni iterazione
% iter: numero di iterazioni

bnrm2 = norm(b);
xv = x0;
r = b - A*x0;

iter=0;

x=x0;

zv=[0 0]';

while(iter<nmax && norm(r)/bnrm2 > toll )
    if nargin==7
        z=Prec\r;
    else
        z=r;
    end
    %if (z'*zv)>1e-12
    %    error('direzioni non ortogonali')
    %end
    zv=z;
    if isnan(alpha_in)
        alpha = (z'*r)/( z'*(A*z) );
    else
        alpha=alpha_in;
    end
    xn =  xv + alpha * z;
    xv=xn;
    x=[x, xn];
    r = r - alpha * A*z;
    iter=iter+1;
end
