function sol = es12(a,b,k)
n=mod(k,4)+3;
sol=0;
for i=0:n
	sol=sol+factorial(n)/factorial(i)/factorial(n-i).*a^i.*b^(n-i);
end
