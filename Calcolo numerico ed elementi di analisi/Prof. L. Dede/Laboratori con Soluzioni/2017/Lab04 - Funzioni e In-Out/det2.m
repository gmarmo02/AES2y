function [ determinant ] = det2( A )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[n,m] = size(A);

if (n==m && m ==2)
        determinant = A(1,1)*A(2,2) - A(1,2)*A(2,1);
end

end

