function [ dY ] = missile( t, Y, settings )

x = Y(1);
z = Y(2);
theta = Y(3);
u = Y(4);
w = Y(5);
q = Y(6);
m = Y(7);
Iy = Y(8);


CD = settings.CD;
CLa = settings.CLa;
CMa = settings.CMa;
S = settings.S;
d = settings.d;
tb = settings.tb;
g = 9.81;
v_wind = settings.v_wind;

if t<tb
    T = settings.T;
    I_dot = settings.Idot;
    mfr = settings.mfr;  
else
    T = 0;
    I_dot = 0;
    mfr = 0;
end


% Alpha
ur = u - v_wind(1);
wr = w - v_wind(2);
V = [ur wr]';



if abs(V(1)) <= 1e-4 % in order to avoid round-off errors we set it to zero
    alpha = 0;
else
    alpha = theta - atan( V(2)/V(1) );
end



% Rotations Matrices
R_ib = [cos(theta) sin(theta);
        sin(theta) -cos(theta)];
R_wb = [cos(theta) sin(theta);
        -sin(theta) cos(theta)];


if z < 0 
    z = 0;
end

% Forces
[~,~,~, rho] = atmosisa(z);

D = 0.5*rho*norm(V)^2*S*CD;
L = 0.5*rho*norm(V)^2*S*CLa*alpha;
M = 0.5*rho*norm(V)^2*S*CMa*alpha;

F_w = [-D -L]';

F_b = R_wb*F_w + [T 0]';

F = R_ib'*F_b + [0 -m*g]';


%% Accelerations
du = F(1)/m + mfr*u/m;
dw = F(2)/m + mfr*w/m;
dq = M/Iy + I_dot*q/Iy;




%% Variables to integrate
dY(1) = u;
dY(2) = w;
dY(3) = q;
dY(4) = du;
dY(5) = dw;
dY(6) = dq;
dY(7) = -mfr;
dY(8) = -I_dot;

dY = dY';

end