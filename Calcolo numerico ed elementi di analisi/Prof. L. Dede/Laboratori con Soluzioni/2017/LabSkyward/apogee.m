function [ value, isTerminal, direction] = apogee( t, Y, settings )

value = Y(5);

direction = 0; % +1 climb, 0 apogee, -1 descent

if t > settings.tb
    isTerminal = 1;
else
    isTerminal = 0;
end



end

