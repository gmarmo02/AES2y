function [U,griglia_x,passi_t]=diff_fin(f,aa,bb,us,ud,u0,T,n,deltat,theta,make_plot)

%     U = diff_fin(f,aa,bb,us,ud,u0,T,n,deltat,theta,make_plot)
%------------------------------------------
% risolve l'equazione
%
% u_t - u_xx  = f(x,t)
%
% con dati al bordo:
%
% u(aa,t)=us(t)
% u(bb,t)=ud(t)
%
% e condizione iniziale
%
% u(x,0)=u0(x)
% 
% per  t <= T
%------------------------------------------
% i parametri in ingresso sono:
%
% f -> forzante, restituisce ad ogni istante t il 
%      vettore contenente la valutazione di f sui nodi x.
%      Deve restituire un vettore colonna. (es f=@(x,t) t*x')
%
% aa -> estremo sinistro del dominio
% bb -> estremo destro del dominio
% us -> funzione che assegna il dato al bordo di sinistra  
%       ( es: us=@(t) 3*t )
% ud -> funzione che assegna il dato al bordo di destra 
%       ( es: ud=@(t) 3*t )
% u0 -> funzione che assegna il dato iniziale. Deve restituire un 
%       vettore colonna (es: u0= @(x) x'-5 )
%
% T -> istante finale dell'intervallo temporale di risoluzione
%
% n -> numero di nodi interni (ovvero dimensione del sistema lineare) 
%      ( (n+1) intervalli, (n+2) nodi totali )
%
% deltat -> passo di avanzamento temporale;
%
% theta -> parametro theta per l'avanzamento in tempo: 
%          theta=0 per EA, theta=0.5 per CN, theta=1 per EI;
%
% make_plot -> flag per attivare(make_plot=1)/disattivare(make_plot=0) 
%              il plot della soluzione (per mesh fitte e' raccomandato 
%              make_plot=0, essendo un'operazione estremamente costosa)
%
%------------------------------------------
% parametri in uscita:
%
% U -> matrice ottenuta accostando per colonne le soluzioni u calcolate ad 
%      ogni istante, le cui dimensioni sono (n+2) x floor(T/deltat)
%
% griglia_x -> il vettore dei nodi spaziali in cui la soluzione viene
%              calcolata
%
% passi_t -> il vettore degli istanti temporali in cui la soluzione viene
%            calcolata

Q=0;
                                        
% griglia di calcolo

h=(bb-aa)/(n+1);

griglia_x=aa:h:bb;

% costruisco le diagonali della matrice A dei nodi interni 

% diagonale principale
d0=(2/h^2+Q)*ones(n,1);

% sovradiagonale
du=-1*ones(n-1,1)/h^2; 
% sottodiagonale
dl=du;       

% theta metodo
%---------------------------------------

% modifichiamo per creare le diagonali della 
% matrice A_theta =d I + deltat*theta*A

d0_theta=theta*deltat*d0+ones(n,1);
du_theta=theta*deltat*du;
% sottodiagonale
dl_theta=du_theta;       

% costruisco la matrice che moltiplica u_old nel termine noto: 

d0_1_theta=ones(n,1)-(1-theta)*deltat*d0;
du_1_theta=-(1-theta)*deltat*du;
% sottodiagonale
dl_1_theta=du_1_theta;       

A_1_theta=sparse(diag(d0_1_theta)+diag(du_1_theta,1)+diag(dl_1_theta,-1));

% ciclo sui passi temporali
%--------------------------------------

% se divido T in N_t passi allora avro' N_t+1 vettori u, 
% contando anche la condizione iniziale
N_t=ceil(T/deltat); 

passi_t=0:deltat:deltat*N_t;

%u_old=zeros(n,1);
u_old = u0(griglia_x(2:end-1) );

% U e' una matrice che contiene le soluzioni u a tutti gli istanti, 
% accostate per colonne
U=zeros(n+2,N_t+1);
U(:,1)=[us(0); u_old; ud(0)];

for iter=1:N_t
    
    b=zeros(n,1);
    
    % costruisco il termine noto b, formato da un termine dovuto ad u_old
    %
    % A_1_theta*u_old
    %
    % e da un termine dovuto alla forzante esterna f
    %
    % deltat* ( theta*f + (1-theta)*f_old  )
    %
    
    % costruisco il contributo di theta_old
    %-----------------------------------------

    b=A_1_theta*u_old;
    
    
    % aggiungo il termine della forzante
    %-----------------------------------------
 
    % prima la forzante 
    
    f_star=zeros(n,1);
    f_star_old=zeros(n,1);
    
    f_star=f(griglia_x(2:end-1),deltat*iter);
    f_star_old=f(griglia_x(2:end-1),deltat*(iter-1));
    
    % devo ricordarmi i termini di bordo, che sono noti 
    % (ed essendo costanti non sono influenzati dal theta metodo)
    % ad entrambi gli istanti
    
    f_star(1)=f_star(1)+us(deltat*iter)/h^2;
    f_star(end)=f_star(end) + ud(deltat*iter)/h^2;

    f_star_old(1)=f_star_old(1)+us( deltat*(iter-1) ) / h^2;
    f_star_old(end)=f_star_old(end) +ud( deltat*(iter-1) ) / h^2;

    % combino i due contributi
    %---------------------------------------
    
     b= b + deltat* ( theta*f_star + (1-theta)*f_star_old  );
    
    % risolvo il sistema lineare con Thomas; 
    % il vettore delle incongnite si chiama u    
    %-----------------------------------------
    %-----------------------------------------

    [LT,UT,u]=thomas(du_theta,dl_theta,d0_theta,b);

    U(:,iter+1)=[us(deltat*iter); u; ud(deltat*iter)];
    
    % aggiorno u_old 
    
    u_old=u;
           
end

%disp('finito il calcolo di U. Preparo il grafico')

if make_plot==1

      [T,X]=meshgrid(passi_t,griglia_x);

      figure
      surf(X,T,U,'LineStyle','none')

      xlabel('x')
      ylabel('t')
      zlabel('u')
      title(strcat('theta=',num2str(theta)),'fontsize',16)

end