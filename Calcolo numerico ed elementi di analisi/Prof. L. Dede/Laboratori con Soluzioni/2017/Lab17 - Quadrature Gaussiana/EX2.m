close all

f = @(x) 1./(x+2);
g = @(x) sqrt(x);
a = 0; b = 1;

n_vect = [2:8];

I_f_vect = [];
I_g_vect = [];

I_T_f_vect = [];
I_T_g_vect = [];


for n = min(n_vect):max(n_vect)

    [knots, weights] = zplege(n, a, b);
    
    I_f_ex = dot( weights, f(knots) );
    I_g_ex = dot( weights, g(knots) );

    I_f_vect = [I_f_vect; I_f_ex];
    I_g_vect = [I_g_vect; I_g_ex];
    
    
    
   % T
   M = n;
   I_T_f = trapcomp( a, b, M, f );
   I_T_f_vect = [I_T_f_vect; I_T_f];
   I_T_g = trapcomp( a, b, M, g );
   I_T_g_vect = [I_T_g_vect; I_T_g];
   
end

I_f_ex = log(1.5);
I_g_ex = 2/3;


%GL
e_vect_f = abs(I_f_ex - I_f_vect)
e_vect_g = abs(I_g_ex - I_g_vect)

semilogy(n_vect, e_vect_f)
hold on
grid on
semilogy(n_vect, e_vect_g)

%T
e_vect_T_f = abs(I_f_ex - I_T_f_vect)
e_vect_T_g = abs(I_g_ex - I_T_g_vect)
semilogy(n_vect, e_vect_T_f)
semilogy(n_vect, e_vect_T_g)

set(0,'defaultTextInterpreter','latex')

legend('e_{f}^{(GL)}','e_{g}^{(GL)}','e_{f}^{(T)}','e_{g}^{(T)}')

