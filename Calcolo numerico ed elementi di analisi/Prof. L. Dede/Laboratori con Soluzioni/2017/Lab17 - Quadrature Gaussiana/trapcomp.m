function [ I ] = trapcomp( a, b, M, f )

H = (b-a)/M;

x_vect = linspace(a,b,M+1);

bPlusB_vect = [];

for k = 1:M
    bPlusB_vect = [bPlusB_vect (f(x_vect(k)) + f(x_vect(k+1)))];
end

I = (H/2)*sum(bPlusB_vect);



return