clear all
close all

%% PageRank Algorithm (S. Brin, L. Page)


%% EX 1

%% 1.1

n = 100;

A = randi([0,1], n);


for j=1:n
   
    sumColumn = sum(A(:,j));
    A(:,j) = A(:,j)/sumColumn;
    
end
A


%% 1.2

B = [0 0 0 1 0;
    1 0 0 0 0;
    0 1 0 0 0;
    0 1 0 0 1;
    1 1 1 1 0];

for j=1:5
   
    sumColumn = sum(B(:,j));
    B(:,j) = B(:,j)/sumColumn;
    
end
B


%% N.B.:   .' -> Congiugato sempre; mentre ' fa il trasposto se matrice reale, se complessa fa l'hermitiano



%% 1.4

tol = 1e-6;
itermax = 1e+3;
x0 = (1/n)*ones(n,1);
[ lambda_A, x_A, k_A ] = eigpower( A, tol, itermax, x0 )

x0 = (1/5)*ones(5,1);
[ lambda_B, x_B, k_B ] = eigpower( B, tol, itermax, x0 )



%% EX. 2

A = toeplitz(1:4);
x0 = [1 2 3 4]';
itermax = 1000;



[mu,x,k1] = invpower(A,tol ,itermax,x0)


% x0 = [1 1 1 1]';
% [mu,x,k2] = invpower(A,tol ,itermax,x0)

