close all
%% EX 1

%% 1.1

f = @(x) atan(7*(x-pi/2)) + sin((x-pi/2).^3);

x = [-1:.001:6];

figure('Name','f(x) = atan(7*(x-pi/2)) + sin((x-pi/2).^3)','NumberTitle','on');
plot(x,f(x));
xlabel('x')
ylabel('f(x)')
hold on;


grid on;


%% 1.2
disp('Metodo Newton')
der_f = @(x) 7./(1+(7*(x-pi/2)).^2) + 3.*(x-pi/2).^2.*cos((x-pi/2).^3);hold on
%plot(x,der_f(x))

alpha_exact = pi/2;
if (der_f(alpha_exact) ~= 0) 
    disp('Lo zero alpha = pi/2 e'' semplice')
else
    disp('Lo zero alpha = pi/2 non e'' semplice')
end


tol = 10e-10;
x0 = 1.5;
itermax = 1000;
[x_vect1,k1] = newton(x0,itermax,tol,f,der_f);
% Osserviamo che questa x0 ci fa CONVERGERE
sprintf('Con x0=%d Newton converge a: %d in %d iterazioni', x0, x_vect1(end), k1)
err = abs(x_vect1(end) - alpha_exact);
sprintf('Errore = %d', err)

x0 = 4;
[x_vect2,k2] = newton(x0,itermax,tol,f,der_f);
% Osserviamo che questa x0 ci fa DIVERGERE
sprintf('Con x0=%d Newton diverge', x0)


%% 1.3
disp('Metodo Bisezione')
a = -1;
b = 6;
tol = (b-a)/(2^31);

[x_vect, k] = bisect(a,b,tol,f);
sprintf('Con x0=%d Newton converge a: %d in %d iterazioni', x0, x_vect(end), k)

err = (x_vect(end) - alpha_exact);

sprintf('Errore = %d', err)


%% 1.4
disp('Bisezione + Newton')

itermax_b = 5;
itermax_n = 1000;
[ x_vect, k ] = bisectNewton(a,b,itermax_b,itermax_n,tol,f,der_f)



%% EX. 2
disp('Punto fisso')

%% 2.1
f = @(x) (cos(2*x)).^2 - x.^2;

figure('Name','Punto Fisso','NumberTitle','on');
subplot(1,2,1)
plot (x,f(x))
xlabel('x')
ylabel('\phi(x)')
grid on

%% 2.2
% φ(x) = x + Af(x)
% |φ'(α)| < 1 (th Ostrowsky)
% ovvero φ'(α) = 1 + A*f'(α) < 1
% |A * (-2*sin(2*x)*2.*cos(2*x) - 2x)| < 1; ---> 0 < A < -2/f'(α)


%% 2.3
A = 0.1;
phi = @(x) x + A*f(x);

x0 = 0.1;
itermax = 1000;
tol = 1e-10;

[succ, k] = fixedPt(x0, phi, itermax, tol);
subplot(1,2,2)
plot([1:k],succ)
xlabel('k')
ylabel('\alpha^{(k)}')
grid on


der_f = @(x) -2*sin(2*x)*2.*cos(2*x) - 2*x;

A_min = 0
A_max = -2/der_f(succ(end))


step = 0.01;
A_vect = [];
k_vect = [];
for A = 0.1:step:floor(10*A_max)/10
    A_vect = [A_vect A];
    phi = @(x) x + A*f(x);
    [~, k] = fixedPt(x0, phi, itermax, tol);
    k_vect = [k_vect k]; 
end


figure('Name','Punto Fisso','NumberTitle','on');
subplot(1,2,1)
plot (A_vect, k_vect)
xlabel('A (parameter)')
ylabel('k')
grid on


%% 2.4
stimap(succ);


%% 2.5
% Ho p = 2 se φ'(α) = 0, ovvero se A = -1/f'(α)
disp('Caso A_opt')
A_opt = -1/der_f(succ(end))
phi = @(x) x + A_opt*f(x);
[succ, k] = fixedPt(x0, phi, itermax, tol);
k

subP(1) = subplot(1,2,2);
plot([1:k],succ)
grid on
title(subP(1),'A_{opt} succession (p=2)')


%% 2.6
disp('Newton come punto fisso: phi_n(x) = x - f(x)/der_f(x)')
phi_N = @(x) x - f(x)/der_f(x);


step = 0.01;
x0_vect = [];
k_vect = [];
for x0 = 0.1:step:1.5
    x0_vect = [x0_vect x0];
    [~, k] = fixedPt(x0, phi, itermax, tol);
    k_vect = [k_vect k]; 
end

figure('Name','Newton come Punto Fisso','NumberTitle','on');
plot(x0_vect, k_vect)
xlabel('x^{(0)}')
ylabel('k')
set(gca,'ylim',[0,max(k_vect)]);
grid on

[minimum, index] = min(k_vect);
x0_opt = x0_vect(index)






















