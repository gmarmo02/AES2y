A = hilb(1000);
B = rand(1000);

%% 1.1
x = ones(1000,1);
b = A*x;

y = ones(1000,1);
c = B*y;


%% 1.2
x_n = A\b;
y_n = B\c;


%% 1.3
% OSS.: Persino l'architettura 32/64 bit ha delle enormi influenze sul
% numero di condizionamento di H

e_rx = norm(x - x_n)/norm(x)
K_h = cond(A)

e_ry = norm(y - y_n)/norm(y)
K_r = cond(B)


N = 20
A = zeros(N-1)

n = length(diag(A));

a = rand(n,1)
e = rand(n-1,1)
c = rand(n-1,1)

b = rand(n,1);

%% IMPORTANT
A = diag(a,0) + diag(c,1) + diag(e,-1);


[L,U,x] = thomas(A,b)



%% Springs example
A = -2*eye(n) + diag(ones(n-1,1),1) + diag(ones(n-1,1),-1);
b = zeros(n);
K = 100; L = 20;
b (n) = -K*L;

t0 = tic;

[L,U,x] = thomas(A,b)

t1 = toc(t0)


t2 = tic;

x = A\b;

t3 = toc(t2)



