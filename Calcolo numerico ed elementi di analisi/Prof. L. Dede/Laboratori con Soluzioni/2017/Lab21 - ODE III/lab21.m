
close all
clear all

%% Example: Lotka Volterra
t0 = 0;
tf = 10;
y0 = [2 2];
[t,Y] = ode45('lotka', [t0, tf], y0, settings);
figure
plot(t,Y)
grid on


%% 1.1-2
close all
t0 = 0;
tf = 100;
y0 = [2 0];
[t,Y] = ode45('smd', [t0, tf], y0);
figure
plot(t,Y(:,1))
hold on
plot(t,Y(:,2))
grid on


gamma = 0.1;
omegaSq = 1;


freq = 0.5*sqrt(4*omegaSq - gamma^2) / (2*pi);

plot(t,cos((2*pi*freq)*t))

legend('x', 'x''', 'cos(2\pift)')


%% 1.3
close all

t0 = 0;
tf = 150;
y0 = [2 0];
[t,Y] = ode45('smd_f', [t0, tf], y0);
figure
plot(t,Y(:,1))
hold on
plot(t,Y(:,2))
grid on


gamma = 0.1;
omegaSq = 1;
A_0 = 0.5;
omega_f = 0.5;


freq = omega_f / (2*pi);

plot(t,sin((2*pi*freq)*t))

legend('x', 'x''', 'sin(2\pift)')


%% 1.3 variant (through Function Handle)
close all

%*******************

gamma = 0.1;
omegaSq = 1;
A_0 = 0.5;
omega_f = 0.5;

f = @(t,y) [y(2); - omegaSq*y(1) - gamma*y(2) + A_0*sin(omega_f*t)]



t0 = 0;
tf = 150;
y0 = [2 0];
[t,Y] = ode45(f, [t0, tf], y0); %*******************
figure
plot(t,Y(:,1))
hold on
plot(t,Y(:,2))
grid on


freq = omega_f / (2*pi);

plot(t,sin((2*pi*freq)*t))

legend('x', 'x''', 'sin(2\pift)')

