close all
clear all

%% 1.4, 1.5

f = @(t,y) -y - t.*sinh(2*t)/2 + cosh(2*t).*(1-t);
t0 = 0;
tf = 1;
y_0 = 0;

h_vect = 0.25*2.^-[0:3];

y_ex = @(t) sinh(2*t).*(1-t)/2;

u_f_vect = [];
e_h_vect = [];

for k = 1:length(h_vect)
    h = h_vect(k);
    
    
    [t_h, u_h] = feuler(f,t0,tf,y_0,h);
    
    u_f = u_h(end);
    u_f_vect = [u_f_vect; u_f];
    
    
    e_h = max(abs(y_ex(t_h) - u_h));
    e_h_vect = [e_h_vect; e_h];


end


u_f_vect
e_h_vect


loglog(h_vect, e_h_vect)
grid on
hold on
loglog(h_vect, h_vect, '--k')
legend('e_{h}','h^{1}')
xlabel('h')
ylabel('err')