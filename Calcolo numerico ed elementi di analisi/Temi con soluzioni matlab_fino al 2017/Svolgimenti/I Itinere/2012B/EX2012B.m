close all

%% 1.1
n = 5;
A = diag([1:n]) + diag(sin([2:n]),1) + diag(sin([2:n]),-1);
b = ones(n,1);

if A==A' & min(eig(A)) > 0
   disp('A è SDP')
end

%% 1.2
% Rich. staz. non precond. -> P=I, A entrambe SDP
% Quindi, notando che in questo caso P^1*A=I*A=A

alpha_opt = 2 / ( max(eig(A)) + min(eig(A)) )

%% 1.3
P = eye(n);
x0 = ones(n,1);
tol = 1e-6;
nmax = 1000;


disp('for alpha = alpha_opt = 0.327632472984527')
alpha = alpha_opt;
[ x, k ] = richardson( A, b, P, x0, tol, nmax, alpha );
k

disp('for alpha = 0.03')
alpha = 0.03;
[ x, k ] = richardson( A, b, P, x0, tol, nmax, alpha );
k


%% 1.4
% Essendo P=I -> d = (K(P^-1A)-1) / (K(P^-1A)+1) = (K(A)-1) / (K(A)+1)
K_A = cond(A) %essendo P^-1A=A simmentrica -> K = K2
% (si ricordi che cond(A) calcola il numero di cond. in norma 2)

d = (K_A - 1)/(K_A + 1)



%% 2.1
f = @(x) sqrt(abs(x)).*(x-1.5).*(x+0.5);
x = [-2:0.001:2];
xAxis = zeros(1,length(x));
plot(x,f(x),'r', x,xAxis,'g')
grid on


%% 2.5
figure

tol = 1e-15;

disp('Let''s calculate alpha_1 = -0.5')
a =-2; b =-0.2;
[ x_vect, k, itermax1] = bisect( f,a,b,tol );
k
itermax1
x_vect(end)

n = length(x_vect)
err_vect = [];
for k = 1:n-1
   err_vect = [err_vect abs(x_vect(k+1)-x_vect(k))]
end
semilogy([1:n-1], err_vect)

hold on
grid on

disp('Let''s calculate alpha_1 = 1.5')
a =0.5; b =3;
[ x_vect, k, itermax] = bisect( f,a,b,tol );
k
itermax
x_vect(end)

n = length(x_vect)
err_vect = [];
for k = 1:n-1
   err_vect = [err_vect abs(x_vect(k+1)-x_vect(k))]; 
end
semilogy([1:n-1], err_vect)

legend('Diff. Consecutive Iterations to calculate \alpha_{1}=-0.5', 'Diff. Consecutive Iterations to calculate \alpha_{3}=1.5')


% Osserviamo una convergenza lineare relativamente all'errore
% associato alla differenza tra due iterate successive