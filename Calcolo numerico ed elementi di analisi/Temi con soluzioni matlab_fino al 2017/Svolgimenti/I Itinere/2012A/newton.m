function [ x_vect, k] = newton( x0, itermax, f, df, tol )

if (nargin == 4)
    tol = 1e-6;
end

k = 0;
x = x0;
x_vect = [];

err = tol+1;

while (err > tol) && (k < itermax)
    k = k+1;
    x_old = x;
    x = x - f(x)/df(x);
    x_vect = [x_vect; x];
    
    err = abs(x-x_old);
end


return

