close all

%% 1.1
n = 20;

A = diag([2:2:2*n]) + diag(-sin([n-1:-1:1]),1) + diag(-sin([n-1:-1:1]),-1);

x_ex = ones(n,1);
b = A*x_ex;

K2 = cond(A)


%% 1.2
if det(A)~=0

    % dom. diag stretta righe
    A_abs = abs(A);
    isDom = 1;
    for i = 1:n
        sumRow = A_abs(i,:);
       if A_abs(i,i) <= sumRow
           isDom = 0;
           break;
       end     
    end
    
    if isDom
        disp('SI dom. diag. stretta righe')
    else
        disp('NO dom. diag. stretta righe')
    end
    
    
    % dom. diag stretta colonne
    A_abs = abs(A');
    isDom = 1;
    for i = 1:n
        sumRow = A_abs(i,:);
       if A_abs(i,i) <= sumRow
           isDom = 0;
           break;
       end     
    end
    
    if isDom
        disp('SI dom. diag. stretta colonne')
    else
        disp('NO dom. diag. stretta colonne')
    end
    
    
    % SDP
    if A==A' & min(eig(A))>0
        disp('SI SDP')
    else
        disp('NO SDP')
    end
    
    % Minori di Nord-Ovest
    % Vediamo che tutte le C.S. sono verificate
    % ma basta che almeno una di esse sia verificata
    % ed allora deve necessariamente valere anche la C.N.S
    % comunque verifichiamolo lo stesso:
    
    isDetAkZero = 0;
    
    for k = 1:n-1
        if det(A(1:k,1:k)) == 0
            isDetAkZero = 1;
            break;
        end
    end
    
    if isDetAkZero
        disp('Esiste det(A_k)=0 -> Non esiste LU')
    else
        disp('C.N.S. LU verificata -> Esiste unica fatt. LU')
    end
    
end




%% 1.3

[L,U,P] = lu(A);
y = fwsub(L,P*b);
x = bksub(U,y)


%% 1.4
r = b-A*x;
r_rel = norm(r)/norm(b)

e_rel = norm(x-x_ex)/norm(x_ex)

K2





%% 2.1

f = @(x) 2*x.^3 - 7*x.^2 - exp(-x.^2) + 1
x = [-2:0.001:8];
df = @(x) 6*x.^2 - 14*x + 2*x.*exp(-x.^2);

plot(x,f(x),'r', x,df(x),'b')
legend('f(x)', 'f''(x)')
grid on

alpha_1 = 0;
df_alpha_1 = df(alpha_1)

df2 = @(x) 12*x - 14 - 4*x.^2.*exp(-x.^2);
df_alpha_1 = df2(alpha_1) %non nulla -> m=2;



%% 2.3
x0 = 0.5;
itermax = 100;
[ x_vect, k] = newton( x0, itermax, f, df )

err_vect = abs(alpha_1*ones(k,1) - x_vect);

figure
semilogy([1:k], err_vect)
grid on

