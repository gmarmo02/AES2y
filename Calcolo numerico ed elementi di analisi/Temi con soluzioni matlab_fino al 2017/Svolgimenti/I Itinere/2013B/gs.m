function [x,k] = gs(A,b,x0,tol,itermax)

if (prod(diag(A)) == 0)
    error('det(P_GS)=0')
end

T = tril(A);
x = x0;
r = b-A*x0;
err = norm(r)/norm(b);
k = 0;

while (err > tol) && (k < itermax)
  	k = k+1;
    
  	%z = fwsub(T,r);
    z = T\r;
  	x = x + z;
    r = b - A*x;
    
  	err = norm(r)/norm(b);
end

return

