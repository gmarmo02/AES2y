clear all
close all

f = @(t,y) exp(-5*t)./(1+t.^2) - 5*y;
t0 = 0; tf = 1;
y0 = 0;

y_ex = @(t) exp(-5*t).*atan(t);

%% 2.c

u = y0;

h_vect = 0.1*2.^-[0:3];

e_Nh_vect = [];

for k = 1:length(h_vect)
    h = h_vect(k)
    Nh = (tf-t0)/h;
    
    
    
    for n = 0:Nh-1
        
        tn = t0 + h*n;
        u = u + h*f(tn,u);
    end
    
    u_Nh = u
    e_Nh = y_ex(tn) - u_Nh
    
    e_Nh_vect = [e_Nh_vect; e_Nh];

end

p_estim_1 = log(e_Nh_vect(1)/e_Nh_vect(2)) / log(h_vect(1)/h_vect(2))
p_estim_2 = log(e_Nh_vect(2)/e_Nh_vect(3)) / log(h_vect(2)/h_vect(3))
p_estim_3 = log(e_Nh_vect(3)/e_Nh_vect(4)) / log(h_vect(3)/h_vect(4))


p_samples = [p_estim_1 p_estim_2 p_estim_3];

p = round( sum(p_samples)/length(p_samples) )