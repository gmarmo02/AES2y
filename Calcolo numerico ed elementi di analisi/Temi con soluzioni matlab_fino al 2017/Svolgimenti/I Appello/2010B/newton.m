function [x_vect,k] = newton(x0,itermax,tol,f,df,m)

x = x0;
err = tol + 1;
k = 0;
x_vect = [];

while (err > tol) && (k < itermax)
   k = k+1;
   x_old = x;
   x = x - m*f(x)/df(x);
   x_vect = [x_vect; x];
   err = abs(x-x_old);
end

end

