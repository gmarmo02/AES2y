close all

%% 1.2
n = 30;
A = diag([2:2:2*n]) + diag(sqrt([2:n]),-1) - diag(1./[2:n], 1)
T = 0.8*A;
x_exact = 2*ones(n,1);

b = A*x_exact;

B_J = (eye(n) - inv(diag(diag(A)))*A);
rho_B_J = max(abs(eig(B_J)))

B_GS = (eye(n) - inv(tril(A))*A);
rho_B_GS = max(abs(eig(B_GS)))

B_3 = 0.8*eye(n);
rho_B_3 = max(abs(eig(B_3)))


%% 1.3
x0 = 100*[1:n]';
tol = 1e-6;
itermax = 1e+4;
[ x, k ] = jacobi( A, b, x0, tol, itermax)


%% 1.4
%e_rel <= K2(A)*r_rel -> r_rel = e_rel/K2(A) -> Se voglio limitare l'errore
%sotto 1e-6 -> tol = 1e-6/K2(A)

tol = 1e-6/cond(A)

%infatti
[ x, k ] = jacobi( A, b, x0, tol, itermax);
e_rel = norm(x-x_exact)/norm(x)



%% 2
f = @(x) exp(1)*log(x)-x;
x = [2:0.001:6];
plot(x,f(x))
grid on
hold on
df = @(x) exp(1)./x-1;
plot(x,df(x))

d2f = @(x) -exp(1)./(x.^2);
plot(x,d2f(x))

legend('f(x)', 'f''(x)', 'f''''(x)')
% --> Graficamente deduciamo che: m = 2

m=2;
itermax = 100;
x0 = 6;
tol = 1e-5;
[x_vect,k] = newton(x0,itermax,tol,f,df,m)