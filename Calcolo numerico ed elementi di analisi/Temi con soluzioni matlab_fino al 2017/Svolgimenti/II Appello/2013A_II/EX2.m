clear
clc

a = -pi; b = pi;
x = linspace(a,b,1000);
f = @(x) ((x-1).^2).*sin(x - pi/2);
df = @(x) cos(x - pi/2).*(x - 1).^2 + sin(x - pi/2).*(2*x - 2);
d2f = @(x) 2*sin(x - pi/2) + 2*cos(x - pi/2).*(2*x - 2) - sin(x - pi/2).*(x - 1).^2;
%% 1
figure
plot(x,f(x),x,zeros(1,1000),x,df(x),x,d2f(x))
legend('f','zeros','df','d2f')

%% 3
nmax = 100;
tol = 1e-06;
x0 = -1;
[xvectS,itS]=newton(x0,nmax,tol,f,df,1)

x0 = 0.5;
[xvectM,itM]=newton(x0,nmax,tol,f,df,2)

%% 4
alphaS = -1.57079633;
errS = abs(xvectS-alphaS)

alphaM = 1.00000000;
errM = abs(xvectM-alphaM)

itS = [1:length(xvectS)];
figure
semilogy(itS,errS)

itM = [1:length(xvectM)];
figure
semilogy(itM,errM)