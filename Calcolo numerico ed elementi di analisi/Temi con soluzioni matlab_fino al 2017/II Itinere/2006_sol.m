% ese1.2

f = '2/sqrt(pi)*exp(-x.^2)';
a =0; b =2;
I = erf(2);
I_simp =[];I_trap=[];
k =[2:10];
for i = k
    I_simp = [I_simp; simpcomp(a,b,i,f)];
    I_trap = [I_trap; trapcomp(a,b,i,f)];
end
I_Simp10 = I_simp(end)
I_trap10 = I_trap(end)

pause 

% ese1.3
err_simp = abs(I_simp-I);
err_trap = abs(I_trap-I);
figure(1);
loglog(k,err_simp,'*',k,err_trap,'+')
legend('simp','pmed')

pause

% ese1.4
hold on
loglog(k,k.^-2,'r',k,k.^-4,'b');
legend('simp','pmed','k^{-2}', 'k^{-4}');
hold off

%2.1

dim = 20; 
A = 4*eye(dim)-diag(ones(dim-1,1),1)-diag(ones(dim-1,1),-1);
v = [1:dim]';
C = v*v'+0.1*A;
x_ex = ones(dim,1);
z_ex = ones(dim,1);
b = A*x_ex;
d = C*z_ex;


%2.2

[LA, UA, PA] = lu(A);
yA = fwsub(LA, PA*b); 
xA_LU = bksub(UA,yA);
errA_LU = norm(x_ex-xA_LU)/norm(x_ex)
resA_LU = norm(b-A*xA_LU)/norm(b)
[LC, UC, PC] = lu(C);
yC = fwsub(LC, PC*d); 
xC_LU = bksub(UC,yC);
errC_LU = norm(x_ex-xC_LU)/norm(x_ex)
resC_LU = norm(d-C*xC_LU)/norm(d)

pause


%2.3

KondA = cond(A)
KondC = cond(C)

pause


%2.5
if (A == A')
    if (eig(A)>0)
       disp('A simmetrica definita positiva');
    else
       disp('A NON definita positiva');
    end
else
    disp('A NON simmetrica');
end

if (C == C')
    if (eig(C)>0)
       disp('C simmetrica definita positiva');
    else
       disp('C NON definita positiva');
    end
else
    disp('C NON simmetrica');
end

pause


%2.6

toll =1e-5;
nmax = 1e5;
x0 = zeros(dim,1);
[xA_GS, iterA_GS] = gauss_seidel(A, b, x0, toll, nmax);
iterA_GS
[xC_GS, iterC_GS] = gauss_seidel(C, d, x0, toll, nmax);
iterC_GS

I = eye(dim);
P_A = tril(A);
B_A = I - inv(P_A)*A;
rhoA_GS = max(abs(eig(B_A)))

P_C = tril(C);
B_C = I - inv(P_C)*C;
rhoC_GS = max(abs(eig(B_C)))

